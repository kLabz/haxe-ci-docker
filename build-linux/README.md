# Build tools for haxe CI

## Registry

https://gitlab.com/kLabz/haxe-ci-docker/container_registry

## Rebuild image

```
docker build -t registry.gitlab.com/klabz/haxe-ci-docker/haxe-ci-docker:0.1.0 .
docker push registry.gitlab.com/klabz/haxe-ci-docker/haxe-ci-docker:0.1.0
```
